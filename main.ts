import { Application, renderFile } from "./deps.ts";

const app = new Application();
const PORT = 8080;
const name = Deno.env.get("PLAYER_NAME") ?? "madanda";

app.renderer = {
  render<T>(name: string, data: T): Promise<Deno.Reader> {
    return renderFile(name, data);
  },
};

app
  .get("/", async (c) => {
    await c.render("./index.html", {
      name,
      image:
        "https://upload.wikimedia.org/wikipedia/fr/thumb/4/43/Logo_Olympique_de_Marseille.svg/1200px-Logo_Olympique_de_Marseille.svg.png",
    });
  })
  .start({ port: PORT });

console.log(`server listening on http://localhost:${PORT}`);
