## ServiceAccount

#### Namespace
Create a new namespace description in a file `develop-ns.yml`:
```yml
apiVersion: v1
kind: Namespace
metadata:
  name: develop
```
then create new namespace:   
`kubectl apply -f develop-ns.yml`  
change the default namespace:  
`kubectl config set-context $(kubectl config current-context) --namespace=develop`


#### ServiceAccount

We going to create a new service account `develop-sa.yml` that we will link to our pods:
```yml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: develop
```
then create them:  
`kubectl apply -f develop-sa.yml`

#### Role

Now we are going to create a role to define permission for our namespace `developer-role.yml`:
```yml
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: develop
  name: developer
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods", "replicasets", "deployments", "configmaps"]
  verbs: ["create", "get", "watch",  "update", "patch", "delete", "list"]
```
then create the resource:  
`kubectl apply -f developer-role.yml`


#### RoleBinding
We are now going to link this role to our service account to grant the permission defined in this our role `developer-rb`:
```yml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: developer-role-binding
  namespace: develop
subjects:
- kind: ServiceAccount
  name: develop 
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role 
  name: developer
  apiGroup: rbac.authorization.k8s.io
```