## Proposition 1
#### Choix du cluster  
Pour simplifier l'accès au cluster utilisé pour ce TP j'ai voulu utiliser le service cloud, Digital Océan qui propose une offre pour la création d'un nouveau compte. Sans succès, je n'ai pas réussi à bénéficier de cette offre. Je me suis donc rabattu sur un cluster locale k3s.

#### Docker in Docker 
Pour builder mon image j'ai choisi d'utiliser Kaniko. Malheureusement, je n'ai pas réussi à pousser mon image sur le Docker hub à l'aide de cet outil dans la CI Gitlab. Une fois l'image builder elle est donc poussé sur le Gitlab Registry de mon projet.

#### Deploy
Du fait que mon cluster se trouve sur ma machine locale, je n'ai pas réussi à y accéder depuis le gitlab.com de ce fait le stage deploy n'a pas été réalisé.